# Microservicio

Microservicio Tiempo

## Requirements

*  Java 1.8

*  Maven

## Install a development environment
Se debe arrancar Sprint boot en la instancia  SpringApplication.run(TiempoApplication.class, args);

## Parametros 
Para la ejecucion del Microservicio
se debe invocar la siguiente URL con los siguientes parametros que se explica a continuacion 

http://localhost:8080/parametros/api/{idCiudad}


*   http://localhost:8080 **-->** Es la URL del Server 

*   parametros **-->** es la invocacion al microservicio  REST

*   {idCiudad} **-->** es el id de la ciudad a desplegar.
 

