package com.tiempo.tiempo.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

@Controller
@RequestMapping(path = "/parametros")
public class TiempoController {

    @RequestMapping("/api/{idCiudad}")
    @CrossOrigin
    public @ResponseBody String obtenerParametros(@PathVariable("idCiudad") String paramCiudad) throws Exception {

       String requestEndpoint= "http://api.tiempo.com/index.php?api_lang=es&localidad="+paramCiudad+"&affiliate_id=hfi5v698dwph&v=3.0";


        try {
            URL url = new URL(requestEndpoint);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            writer.write("Something");
            writer.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer jsonString = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                jsonString.append(line);
            }
            br.close();
            connection.disconnect();
            return jsonString.toString();


        } catch (Exception e) {
           throw new Exception("Error al Procesar idCiudad de ciudad");

        }


    }





}
